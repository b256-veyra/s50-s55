import './App.css';
import { Container } from 'react-bootstrap';
import { useState } from 'react';
/*
The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint.
*/
import { BrowserRouter as Router, Route, Routes, Switch } from 'react-router-dom';
import { UserProvider } from './userContext';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';



function App() {

  // State hook for the user to store the information for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({email: localStorage.getItem('email')});

  // Function for clearing the localStorage when a user logout
  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
       {/*Self Closing Tags */}
      <AppNavBar /> 
      <Container>
        <Routes>
        {/*path is the endpoint and the element is the page*/}
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  )
}

export default App;
