// React Bootstrap Components
import { useContext } from 'react';
import { Container, Navbar, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext';

export default function AppNavBar() {

	// State hooks to store user information in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/">Zuitt Bootcamp</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          	<Nav className="me-auto">
	            	<Nav.Link as={Link} to="/">Home</Nav.Link>
		            <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
		            {
		            	(user.email !== null) ?
		            		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
		            	:
		            	<>
		            		<Nav.Link as={Link} to="/login">Login</Nav.Link>
		            		<Nav.Link as={Link} to="/register">Register</Nav.Link>
		            	</>
		            }	          
	          	</Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}
