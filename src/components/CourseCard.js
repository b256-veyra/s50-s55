import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {
   
    const { name, description, price} = courseProp;

    // getter -> stores the value, variable
    // setter -> it sets the value to be stored in the getter
    // initial Getter Value
    const [count, setCount] = useState(30);

    function enroll() {

        if(count > 0) {

            setCount(count - 1);
            console.log('Seat count: ' + count);
        } else {

            alert('All seats are taken');
        }
    }


    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{count} Students</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
