import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from "../userContext";

export default function Login() {

		const { user, setUser } = useContext(UserContext);

		// State hooks to store the values of our input fields
		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');
		const [isActive, setIsActive] = useState(false);

		console.log(email);
		console.log(password);


		
		// useEffect() - whenever there is a change in our webpage
		useEffect (() => {
			if(email !== '' && password !== '') {

				setIsActive(true)

			} else {

				setIsActive(false)

			}
		})

		function loginUser(e) {

			e.preventDefault();

			// Set the email of the authenticated user in the local storage
			localStorage.setItem("email", email)

			setUser({
				email: localStorage.getItem('email')
			})

			// clear input fields
			setEmail('');
			setPassword('');

			alert('Successful login');
		}

		return (
			(user.email !== null) ?
				<Navigate to="/courses" />
			:
			<Form onSubmit={e => loginUser(e)}>
		      <Form.Group className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
		      </Form.Group>

		      {
		      	isActive ?
		      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		      	:
		      		<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
		      }
		      
		    </Form>
		)
}
