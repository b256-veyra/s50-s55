import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from "../userContext";

export default function Register() {

	const { user, setUser } = useContext(UserContext);
	
	// State hooks to store the value of our input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState(''); 
	const [password2, setPassword2] = useState(''); 
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false)

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect (() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {

			setIsActive(true);

		} else {

			setIsActive(false)

		}
	})

	function registerUser(e) {

		e.preventDefault();

		setUser({
				email: localStorage.getItem('email')
			})

		// clear input fields
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!');
	}

	return (
		(user.email !== null) ?
				<Navigate to="/courses" />
			:
		<Form onSubmit={e => registerUser(e)}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
      </Form.Group>

      {/*Ternary Operator*/}
	  	  {/*
			if (isActive === true) {
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			} else {
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
	  	  */}

      {
      	isActive ?
	      <Button variant="primary" type="submit" id="submitBtn">
	        Submit
	      </Button>
	      :
	      <Button variant="danger" type="submit" id="submitBtn" disabled>
	        Submit
	      </Button>
  		}
    </Form>
	)
}