import React from 'react';

// Create a Context Object
// A context is a data type of an object that can be used to store infiormation that can be shared to other components within the app.
// The context object is a different approach to passing information between components and allows easier by avoiding prop-drilling

const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object and supply the necessary infiromation needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;